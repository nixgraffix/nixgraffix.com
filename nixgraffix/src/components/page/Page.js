import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import ReactGA from 'react-ga'
import './Page.css';
import Home from './components/home/Home.js'
import Resume from './components/resume/Resume.js'
import SwipeSlider from './components/swipeSlider/SwipeSlider.js'
import RatioSlider from './components/ratioSlider/RatioSlider.js'

class Page extends Component {

    render(){

        const history = createHistory()
        history.listen((location, action) => {
            ReactGA.set({ page: location.pathname });
            ReactGA.pageview(location.pathname);
        });


        return (
            <div className="Page">
                <Switch history={history}>
                     <Route exact path='/' component={Home}/>
                     <Route exact path='/resume' component={Resume}/>
                     <Route exact path='/swipe_slider' component={SwipeSlider}/>
                     <Route exact path='/ratio_slider' component={RatioSlider}/>
                </Switch>
            </div>
        );
    }
}

export default Page;