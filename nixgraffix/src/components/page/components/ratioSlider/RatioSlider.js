import React, { Component } from 'react';
//import $ from 'jquery'; 
import './RatioSlider.css';
import bellermann from './assets/bellermann.jpg';
import bush from './assets/bush.jpg';
import ciccareli from './assets/ciccareli.jpg';
import goering from './assets/goering.jpg';
import manifest from './assets/manifest.jpg';
import pissarro from './assets/pissarro.jpg';
import palliere from './assets/palliere.jpg';

class RatioSlider extends Component {

    componentDidMount() {

        const script = document.createElement("script");
        script.src = "/ratioSlider/ratioSlider.js";
        script.async = true;
        script.onload = () => this.scriptLoaded();

        document.body.appendChild(script);

    }

    scriptLoaded() {
        
        window.$(document).ready(function(){
            window.$('.ratioSlider').ratioSlider();
        });
    }

    render(){
        return (
            <div className="RatioSlider">
                <h2>RatioSlider.js</h2>
                <div className="ratioSlider">
                    <img src={bellermann} />
                    <img src={bush} />
                    <img src={ciccareli} />
                    <img src={goering} />
                    <img src={manifest} />
                    <img src={pissarro} />
                    <img src={palliere} />
                </div>
            </div>
        );
    }
}

export default RatioSlider;