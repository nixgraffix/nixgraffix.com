import React, { Component } from 'react';
import './SwipeSlider.css';
import mclaren from './assets/mclaren.jpg';
import porsche from './assets/porsche.jpg';
import f1 from './assets/f1.jpg';
import SwipeSliderWrapper from './SwipeSliderWrapper.js';

class SwipeSlider extends Component {

    render(){
        return (
            <div className="SwipeSlider">
                <h2>SwipeSlider.js</h2>
                <SwipeSliderWrapper>
                        <div className="swipeSliderViewport">
                            <img src={mclaren} />
                        </div>
                        <ul className="swipeSliderSlideSelectors">
                            <li className="slideSelector">
                                <img src={mclaren} />
                            </li>                                                                        
                            <li className="slideSelector">
                                <img src={porsche} />
                            </li>
                            <li className="slideSelector">
                                <img src={f1} />
                            </li>
                        </ul>
                </SwipeSliderWrapper>
            </div>
        );
    }
}

export default SwipeSlider;