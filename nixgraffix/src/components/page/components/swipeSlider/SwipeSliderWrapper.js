import React from 'react';

class SwipeSliderWrapper extends React.Component {
  componentDidMount() {
    this.$el = new window.SwipeSlider(this.el);
    //let swiper = new window.SwipeSlider(document.getElementById('swipeSlider'));
  }

  componentWillUnmount() {
    //this.$el.somePlugin('destroy');
  }

  render() {
    return (
      <div className="swipeSlider" ref={el => this.el = el}>
        {this.props.children}
      </div>
    );
  }
}

export default SwipeSliderWrapper;