import React, { Component } from 'react';
import './Resume.css';

class Resume extends Component {
    render(){
        return (
        <div className="Resume">
            <p className="downloadLinks">Download <a href="/docs/nickArtymiak-reume-2019.pdf" title="Download resume.pdf"><i className="far fa-file-pdf"></i> .pdf</a>  | <a href="/docs/nickArtymiak-reume-2019.docx" title="Download resume.doc"><i className="far fa-file-word"></i> .docx</a></p>
            <h2>Web Developer</h2>
            <ul>
                <li>Over 5 years experience in PHP, MySQL, JavaScript, HTML and CSS.</li>
                <li>Proficient in Linux Administration and Apache Server.</li>
                <li>Able to design overall architecture of a website.</li>
                <li>Experience in building SPA with React and Vanilla JS.</li>
                <li>Practices use of version control systems: Mercurial and GIT.</li>
                <li>Experienced with building and usage of API’s.</li>
                <li>Able to quickly learn new technologies and adapt to new environments.</li>
                <li>Practices good coding fundamentals and has a solid understanding of web technologies.</li>
                <li>Excellent understanding of user-experience and user-flow when designing applications.</li>
                <li>Proficiently uses best coding practices for working well in a team and future code use.</li>
            </ul>

            <h2>Technology</h2>
            <p>PHP • MySQL • HTML • CSS • JavaScript • Node.js • React • JSON • Linux • Apache • Hg • Git</p>

            <h2>Professional Experience</h2>

            <h3>Web Developer</h3>
            <p>Conair Corporation, Stamford, CT<br />
            January 2018-Present</p>

            <h4>WAMP</h4>
            <ul>
                <li>Maintained over 24 corporate sites including flagship site and content management systems.</li>
                <li>Worked closely with designers and project managers using ActiveCollab to track tasks.</li>
                <li>Practiced a deep understanding of OO programming and MVC architecture.</li>
                <li>Designed classes for plugins, features, and the core library to be shared between all sites.</li>
                <li>Built SERP for internal search using Google Search Appliance and ElasticSearch.</li>
                <li>Created and maintained proprietary CMS’s from the ground up.</li>
                <li>Wrote classes to produce XML data for 3rd Party feeds connected by SFTP.</li>
                <li>Designed, produced, and tested RESTful API's using Swagger.</li>
                <li>Proficiently integrated PHP with MySQL database layer.</li>
                <li>Designed MySQL databases and table structures as well as wrote efficient query statements.</li>
                <li>Installed and configured Apache Server in virtual development environment for multiple sites.</li>
                <li>Used CYGWIN and Windows 2016 Server to manage website directories, assets and scripts.</li>
                <li>Connected to UAT and Production environments using SSH.</li>
                <li>Practiced excellent working knowledge of Mercurial VCS to maintain clean repositories and update environments.</li>
                <li>Managed build-processes for Node.js applications such as Foundation, Swagger, and React. </li>
            </ul>

            <h4>Javascript, HTML, and CSS</h4>
            <ul>
                <li>Accurately developed user-experiences as created by designers and stakeholders.</li>
                <li>Created and maintained Javascript apps/modules for DOM manipulation and XHR requests.</li>
                <li>Developed from the ground-up applications for websites using Vanilla JS and React.</li>
                <li>Maintained a fully React website using understanding of routers, state, props, and lifecycles.</li>
                <li>Connected to server using Fetch API with proficient knowledge of server interaction.</li>
                <li>Deep understanding of high order array functions and JSON data structure.</li>
                <li>Proficiently built display layers for multiple sites using HTML and Smarty templating system.</li>
                <li>Coded effectively semantic and dynamic templates for SEO and accessibility purposes.</li>
                <li>Built secure forms for capturing user data and contact information.</li>
                <li>Used up-to-date as well as maintain legacy knowledge of CSS coding practices.</li>
                <li>Practiced building responsive web designs for mobile, tablet, and desktop devices.</li>
            </ul>

            <h4>Extras</h4>
            <ul>
                <li>Worked with Mongo DB.</li>
                <li>Good working knowledge of email client parsing and building with Foundation for emails.</li>
                <li>Knowledge of SalesForce Platform using pipelines and Genesis site creation.</li>
            </ul>

            <h3>Interactive Developer and Designer</h3>
            <p>New Britain Museum of American Art, New Britain, CT<br />
            January 2014-December 2017</p>

            <h4>Javascript</h4>
            <ul>
                <li>Developed and maintained the presentation layer of customer-facing site.</li>
                <li>Experienced in jQuery and writing jQuery plug-ins.</li>
                <li>Used AJAX, XMLHttpRequests, JSON and web tokens for asynchronous server requests.</li>
                <li>Knowledge of React.</li>
                <li>Gathered specs from client to create a feasible workflow for adding new functionality, enhancements, and improvements on the overall website aesthetic.</li>
                <li>Developed and maintained customer facing website, micro-sites, exhibition interactive sites, and content management.</li>
                <li>Created web content, web graphics, and multimedia.</li>
                <li>Monitored site traffic and proactively implemented site enhancements.</li>
            </ul>
            <h4>PHP / MySQL</h4>
            <ul>
                <li>Managed and maintained PHP backend for dynamic database driven websites.</li>
                <li>Proficient in database design and queries.</li>
                <li>Stored and retrieved hashed data.</li>
                <li>Built user profile/login, chat window, and content management system.</li>
                <li>Provided other departments with ability to update website content. Exhibition Interactive Sites</li>
                <li>Worked with curators and artists to create content.</li>
                <li>Designed photoshop comps for layout and user interface. </li>
                <li>Converted comps to website application.</li>
                <li>Developed for mobile use.  Email Marketing</li>
                <li>Managed list organization and subscriber experience in MailChimp.</li>
                <li>Designed and developed templates.</li>
            </ul>

            <p>Earned Award: GDUSA American Web Design Award 2015</p>

            <h3>Web Developer Intern</h3>
            <p>IMPACT Branding and Design, Wallingford, CT<br />
            September 2014-January 2015</p>
            <ul>
                <li>Convert photoshop comps to mobile responsive webpages.</li>
                <li>Fix display issues involving CSS and Javascript.</li>
                <li>Use HubSpot, WordPress.</li>
                <li>Create email marketing advertisements.</li>
            </ul>

            <h2>Education</h2>
            <h3>Central Connecticut State University</h3>
            <p>Bachelor of Arts and Science, Double Major: Computer Science and Graphic Design; GPA 3.24; Activities: President, Computer Science Club; Member, Graphic Design Club</p>

            <h3>Manchester Community College</h3>
            <p>Associate of Arts, Graphic Design; GPA 3.75; Activities: President, Chess Club</p>
        </div>
        );
    }
}

export default Resume;