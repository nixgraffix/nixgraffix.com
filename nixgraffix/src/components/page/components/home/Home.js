import React from 'react'
import './Home.css'

const Home = () => (
    <div className="Home">
        <h2>Hello!</h2>
    </div>
)

export default Home;