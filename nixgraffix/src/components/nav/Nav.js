import React from 'react';
import { Link } from 'react-router-dom'
import './Nav.css';

const Nav = () =>(
    
    <div className="Nav">
        <ul>
            <li>
                <h3>About</h3>
                <ul>
                    <li><Link to='/resume'>Resume</Link></li>
                    <li><a className="external" href="https://www.linkedin.com/in/nickartymiak" target="_blank" rel="noopener noreferrer" title="go to Nick's LinkedIn page">LinkedIn</a></li>
                    <li><a className="external" href="https://github.com/nixgraffix" target="_blank" rel="noopener noreferrer" title="go to Nick's Github page">Github</a></li>
                    <li><a className="external" href="https://bitbucket.org/nixgraffix/" target="_blank" rel="noopener noreferrer" title="go to Nick's Bitbucket page">Bitbucket</a></li>
                </ul>
            </li>
            <li>
                <h3>Websites</h3>
                <ul>
                    <li><a className="external" href="http://conair.la/" target="_blank" rel="noopener noreferrer" title="go to www.conair.la">conair.la</a></li>
                    <li><a className="external" href="http://www.nbmaa.org/nbmaa4/calendar" target="_blank" rel="noopener noreferrer" title="go to nbmaa4 site">nbmaa-v4</a></li>
                    <li><a className="external" href="http://www.nbmaa.org/craft-sippin-2015/index.php" target="_blank" rel="noopener noreferrer" title="go to craft sippin' site">Craft Sippin' (micro site)</a></li>
                    <li><a className="external" href="https://www.markwoollen.com/" target="_blank" rel="noopener noreferrer" title="go to www.markwoollen.com">markwoollen.com</a></li>
                    <li><a className="external" href="http://www.nbmaa.org/" target="_blank" rel="noopener noreferrer" title="go to www.nbmaa.org">nbmaa.org</a></li>
                    
                </ul>
            </li>
            <li>
                <h3>Demos</h3>
                <ul>
                    <li><Link to="/swipe_slider">SwipeSlider.js</Link></li>
                    <li><Link to="/ratio_slider">RatioSlider.js</Link></li>
                    <li><Link>switch.js</Link></li>
                </ul>
            </li>
        </ul>       
    </div>
);

export default Nav;
