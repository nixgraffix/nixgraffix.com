import React, { Component } from 'react'
import ReactGA from 'react-ga'
import Main from './containers/Main'
import './App.css'

class App extends Component {

  constructor(props) {
    super(props);
    
    this.trackingHandler = this.trackingHandler.bind(this);
    
    ReactGA.initialize("UA-142226521-1");
    ReactGA.pageview(window.location.pathname + window.location.search)
  }

  trackingHandler(page) {
    ReactGA.event({
      category: "page_view",
      action: page
    });
  }

  render() {
    return (
      <div>
        <Main/>
      </div>
    );
  }
}

export default App;