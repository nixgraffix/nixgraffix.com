import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Main.css'
import Nav from '../components/nav/Nav'
import Page from '../components/page/Page'

class Main extends Component {

    //state = {}

    render(){
        return(
            <div className="Main">
                <div className="profile">
                    <Link to='/'><h1 className="title">Nick Artymiak / nixgraffix</h1></Link>
                    <p className="jobTitle">Web Developer</p>
                    <p className="contact"><a href="mailto:nicolaiarty@gmail.com">nicolaiarty@gmail.com</a></p>
                </div>
                <div className="content">
                    <Nav/>
                    <Page/>
                </div>
            </div>
        );
    }
}

export default Main;
